import re

def test_patterns(text, patterns=[]):
    """Given source text and a list of patterns, look for
    matches for each pattern within the text and print
    them to stdout.
    """
    # Show the character positions and input text
    print
    print ''.join(str(i/10 or ' ') for i in range(len(text)))
    print ''.join(str(i%10) for i in range(len(text)))
    print text

    # Look for each pattern in the text and print the results
    for pattern in patterns:
        print
        print 'Matching "%s"' % pattern
        for match in re.finditer(pattern, text):
            s = match.start()
            e = match.end()
            print '  %2d : %2d = "%s"' % \
                (s, e-1, text[s:e])
    return

if __name__ == '__main__':
    test_patterns('abbaaabbbbaaaaa', ['ab'])
    test_patterns('abbaaabbbbaaaaa',
              [ 'ab*',    #a followed by a number of b
                'ab+',    #a followed by one or more b
                'ab?',    #optional b at the end of the string
                'ab{3}',  #a followed by 3 b
                'ab{2,3}' #a followed by 2 or 3 b
                ])

    test_patterns('abbaaabbbbaaaaa',
              [ '[ab]',  #a followed by b
                'a[ab]+', #a followed by any number of ab
                ])
