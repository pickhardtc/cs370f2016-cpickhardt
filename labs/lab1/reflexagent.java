//Claire Pickhardt
//Professor Jumadinova
//Computer Science 370
//Lab 1
//13 September 2016
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.
//This agent is a cat treat dispenser that will give a treat if a cat is present.
//
public class reflexagent{
    public static void main(){
        String cat = "here";
        String request = "yes";
        String release;
        //is cat present in front of agent?
        if(cat == "here"){
            //did cat request treat?
            {
                if(request == "yes"){
                    release = "have a treat";
                }
                else{
                    release = "no treat for you";
                }
            }
        }
        else{
            System.exit(0);
        }
    }
}

